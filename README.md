# Adventures in Texture Classification II

Camille Rosewright, M.S. Computer Science, DePaul University

CSC 482: Applied Image Analysis – Texture Classification Project Final Paper

### Abstract

There are a variety of methods that have been researched regarding texture classification. Texture image feature extraction using Haar and Gabor approaches were experimented with in this project for use as input to a Support Vector Machine (SVM) algorithm in order to classify different textures. Both the Haar and Gabor methodologies were good at classifying some of the chosen texture classes, and the results were compared with previous work which used the Local Binary Pattern (LBP) method as the SVM input features. The LBP method has overall greater accuracy, and the Haar and Gabor methods have greater accuracy for specific textures. A combination of the proposed methodologies, as well as an increase in the number of features extracted, will be pursued in future work.

[Read the full text here](https://gitlab.com/thewayitis/texture-classification/-/blob/main/Adventures%20in%20Texture%20Classification%20II.pdf)
