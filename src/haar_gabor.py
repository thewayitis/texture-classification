from skimage.io import imread
from skimage.filters import gabor

from skimage.transform import integral_image, resize
from skimage.feature import haar_like_feature

from sklearn.svm import SVC

import numpy as np

print("Program running...")

# 120 training images for each texture
train_features = []
train_labels = np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
                         1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,\
                         1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,\
                         1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,\
                         1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,\
                         1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,\
                         1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,\
                         2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,\
                         2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,\
                         2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,\
                         2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,\
                         2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,\
                         2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,\
                         3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,\
                         3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,\
                         3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,\
                         3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,\
                         3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,\
                         3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,\
                         4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,\
                         4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,\
                         4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,\
                         4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,\
                         4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,\
                         4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,\
                         5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,\
                         5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,\
                         5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,\
                         5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,\
                         5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,\
                         5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,\
                         6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,\
                         6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,\
                         6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,\
                         6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,\
                         6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,\
                         6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,\
                         7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,\
                         7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,\
                         7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,\
                         7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,\
                         7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,\
                         7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,\
                         8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,\
                         8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,\
                         8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,\
                         8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,\
                         8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,\
                         8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,\
                         9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,\
                         9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,\
                         9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,\
                         9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,\
                         9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,\
                         9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9])

texture_names = ['Canvas', 'Cushion', 'Grass', 'Lentils', 'Linseeds',\
                 'Oatmeal', 'Pearl Sugar', 'Rice', 'Sand', 'Stone']

image_size = (50,50)
# haar filter
detection_window = [30,30]
# gabor filter
freq = 8.0
# feature types: 'type-2-x', 'type-2-y', 'type-3-x', 'type-3-y', 'type-4'
feature_type = ['type-4']
print("Training labels created...")

# extract haar and gabor features
# https://github.com/scikit-image/scikit-image/blob/master/skimage/feature/haar.py#L87

# canvas
canvas_path = 'data/Kylberg/canvas/'
print("Extracting canvas features...")
for i in range(1,121):
    image_path = canvas_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)

# cushion
cushion_path = 'data/Kylberg/cushion/'
print("Extracting cushion features...")
for i in range(1,121):
    image_path = cushion_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)

# grass    
grass_path = 'data/Kylberg/grass/'
print("Extracting grass features...")
for i in range(1,121):
    image_path = grass_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)
 
# lentils    
lentils_path = 'data/Kylberg/lentils/'
print("Extracting lentils features...")
for i in range(1,121):
    image_path = lentils_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
   
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)

# linseeds
linseeds_path = 'data/Kylberg/linseeds/'
print("Extracting linseeds features...")
for i in range(1,121):
    image_path = linseeds_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)
    
# oatmeal
oatmeal_path = 'data/Kylberg/oatmeal/'
print("Extracting oatmeal features...")
for i in range(1,121):
    image_path = oatmeal_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)

# pearl sugar
pearlsugar_path = 'data/Kylberg/pearlsugar/'
print("Extracting pearl sugar features...")
for i in range(1,121):
    image_path = pearlsugar_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)
    
# rice
rice_path = 'data/Kylberg/rice/'
print("Extracting rice features...")
for i in range(1,121):
    image_path = rice_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)

# sand
sand_path = 'data/Kylberg/sand/'
print("Extracting sand features...")
for i in range(1,121):
    image_path = sand_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)
    
# stone
stone_path = 'data/Kylberg/stone/'
print("Extracting stone features...")
for i in range(1,121):
    image_path = stone_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)
    train_features.append(feature_list)

# TRAINING PHASE            
print("Training features: {}".format(np.array(train_features).shape))
print("Training labels: {}".format(train_labels.shape))
svm = SVC(gamma='auto')
svm.fit(train_features, train_labels)
print("Training of SVM complete.")
print()

# TESTING PHASE 
# 40 test images for each of the ten classes  
nums = range(1, 41)

# canvas   
canvas_test_path = canvas_path + 'canvas1-d-p0'
canvas_test_features = []    
for i in nums:
    image_path = canvas_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    canvas_test_features.append(feature_list)
prediction = svm.predict(canvas_test_features)
print(texture_names[0], "Prediction: ", prediction)

# cushion    
cushion_test_path = cushion_path + 'cushion1-d-p0'
cushion_test_features = []  
for i in nums:
    image_path = cushion_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    cushion_test_features.append(feature_list)
prediction = svm.predict(cushion_test_features)
print(texture_names[1], "Prediction: ", prediction)
    
# grass    
grass_test_path = grass_path + 'grass1-d-p0'
grass_test_features = []  
for i in nums:
    image_path = grass_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    grass_test_features.append(feature_list)
prediction = svm.predict(grass_test_features)
print(texture_names[2], "Prediction: ", prediction)

# lentils   
lentils_test_path = lentils_path + 'lentils1-d-p0'
lentils_test_features = []  
for i in nums:
    image_path = lentils_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    lentils_test_features.append(feature_list)
prediction = svm.predict(lentils_test_features)
print(texture_names[3], "Prediction: ", prediction)

# linseeds   
linseeds_test_path = linseeds_path + 'linseeds1-d-p0'
linseeds_test_features = []    
for i in nums:
    image_path = linseeds_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    linseeds_test_features.append(feature_list)
prediction = svm.predict(linseeds_test_features)
print(texture_names[4], "Prediction: ", prediction)

# oatmeal  
oatmeal_test_path = oatmeal_path + 'oatmeal1-d-p0'
oatmeal_test_features = []    
for i in nums:
    image_path = oatmeal_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    oatmeal_test_features.append(feature_list)
prediction = svm.predict(oatmeal_test_features)
print(texture_names[5], "Prediction: ", prediction)

# pearl sugar   
pearlsugar_test_path = pearlsugar_path + 'pearlsugar1-d-p0'
pearlsugar_test_features = []    
for i in nums:
    image_path = pearlsugar_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    pearlsugar_test_features.append(feature_list)
prediction = svm.predict(pearlsugar_test_features)
print(texture_names[6], "Prediction: ", prediction)

# rice  
rice_test_path = rice_path + 'rice1-d-p0'
rice_test_features = []    
for i in nums:
    image_path = rice_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    rice_test_features.append(feature_list)
prediction = svm.predict(rice_test_features)
print(texture_names[7], "Prediction: ", prediction)

# sand   
sand_test_path = sand_path + 'sand1-d-p0'
sand_test_features = []    
for i in nums:
    image_path = sand_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    sand_test_features.append(feature_list)
prediction = svm.predict(sand_test_features)
print(texture_names[8], "Prediction: ", prediction)

# stone  
stone_test_path = stone_path + 'stone3-d-p0'
stone_test_features = []    
for i in nums:
    image_path = stone_test_path + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_image = resize(texture_image, image_size, \
                           mode='constant', anti_aliasing=True)
    
    texture_ii = integral_image(texture_image)
    haar_feature = haar_like_feature(texture_ii, 0, 0, \
        detection_window[0], detection_window[1], feature_type)    
    gabor_feature, imaginary = gabor(texture_image, frequency=freq, mode='wrap')
    
    feature_list = []
    for x in haar_feature:
        feature_list.append(x)
    feature = np.mean(gabor_feature)
    feature_list.append(feature)
    feature = np.var(gabor_feature)
    feature_list.append(feature)    
    stone_test_features.append(feature_list)
prediction = svm.predict(stone_test_features)
print(texture_names[9], "Prediction: ", prediction)
    
# score
canvas_test_labels = np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
cushion_test_labels = np.array([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,\
                                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
grass_test_labels = np.array([2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,\
                              2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2])   
lentils_test_labels = np.array([3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,\
                                3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3])
linseeds_test_labels = np.array([4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,\
                                 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4])
oatmeal_test_labels = np.array([5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,\
                               5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5])
pearlsugar_test_labels = np.array([6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,\
                                  6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6])
rice_test_labels = np.array([7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,\
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7])
sand_test_labels = np.array([8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,\
                             8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8])
stone_test_labels = np.array([9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,\
                              9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9])

canvas_score = svm.score(canvas_test_features, canvas_test_labels)
cushion_score = svm.score(cushion_test_features, cushion_test_labels)   
grass_score = svm.score(grass_test_features, grass_test_labels)
lentils_score = svm.score(lentils_test_features, lentils_test_labels)
linseeds_score = svm.score(linseeds_test_features, linseeds_test_labels)
oatmeal_score = svm.score(oatmeal_test_features, oatmeal_test_labels)
pearlsugar_score = svm.score(pearlsugar_test_features, pearlsugar_test_labels)
rice_score = svm.score(rice_test_features, rice_test_labels)
sand_score = svm.score(sand_test_features, sand_test_labels)
stone_score = svm.score(stone_test_features, stone_test_labels)

print()
print("Canvas score: ", canvas_score)
print("Cushion score: ", cushion_score) 
print("Grass score: ", grass_score)
print("Lentils score: ", lentils_score)   
print("Linseeds score: ", linseeds_score)
print("Oatmeal score: ", oatmeal_score)
print("Pearl sugar score: ", pearlsugar_score)
print("Rice score: ", rice_score)
print("Sand score: ", sand_score)
print("Stone score: ", stone_score)
    
    
    
    
    
    