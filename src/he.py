import matplotlib.pyplot as plt
import cv2
from PIL import Image, ImageEnhance

# read in the image
img = cv2.imread('catPC.jpg', 0)

# display the image
cv2.imshow("image", img)

# create the histogram and modify axis labels
plt.hist(img.flatten(), 256, [0, 256])
plt.xlim([0, 256])
plt.ylim([0, 20000])

# display the histogram
plt.show()

# histogram equalization
eq_img = cv2.equalizeHist(img)

# save the combined image
cv2.imwrite('eq_image.png', eq_img)

# create the histogram and modify axis labels
plt.hist(eq_img.flatten(), 256, [0, 256])
plt.xlim([0, 256])
plt.ylim([0, 20000])

# display the histogram
plt.show()

# apply a local enhancement approach on this image and show your results
# open the image
image = Image.open('catPC.jpg')

# create enhancer for image
enhancer = ImageEnhance.Sharpness(image)

# determine enhancement factor
en_img = enhancer.enhance(4.0)

# save the image
en_img.save('enhanced_image.png')
