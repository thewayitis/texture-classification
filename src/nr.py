from skimage.util import random_noise
from skimage.io import imread, imsave
from skimage.filters.rank import mean, median
from skimage.morphology import disk
import matplotlib.pyplot as plt


# add noise to equalized image from problem 1
img = imread('img.png', as_gray=True)

gaussian_img = random_noise(img, mode='gaussian', var=0.05)
salt_pepper_img = random_noise(img, mode='s&p')

original_image = imread('catPC.jpg', as_gray=True)

# uncomment to use methods
# =============================================================================
# plt.figure(figsize=(12, 8))
# plt.subplot(221)
# plt.imshow(original_image)
# 
# plt.subplot(222)
# plt.imshow(img)
# 
# plt.subplot(223)
# plt.imshow(gaussian_img)
# 
# plt.subplot(224)
# plt.imshow(salt_pepper_img)
# =============================================================================

# =============================================================================
# salt_pepper3 = mean(salt_pepper_img, disk(1))
# salt_pepper5 = mean(salt_pepper_img, disk(2))
# salt_pepper7 = mean(salt_pepper_img, disk(3))
# 
# gaussian3 = mean(gaussian_img, disk(1))
# gaussian5 = mean(gaussian_img, disk(2))
# gaussian7 = mean(gaussian_img, disk(3))
# =============================================================================

# =============================================================================
# plt.figure(figsize=(12, 8))
# plt.subplot(221)
# plt.imshow(salt_pepper_img)
# 
# plt.subplot(222)
# plt.imshow(salt_pepper3)
# 
# plt.subplot(223)
# plt.imshow(salt_pepper5)
# 
# plt.subplot(224)
# plt.imshow(salt_pepper7)
# =============================================================================

# =============================================================================
# plt.figure(figsize=(12, 8))
# plt.subplot(221)
# plt.imshow(gaussian_img)
# 
# plt.subplot(222)
# plt.imshow(gaussian3)
# 
# plt.subplot(223)
# plt.imshow(gaussian5)
# 
# plt.subplot(224)
# plt.imshow(gaussian7)
# =============================================================================

salt_pepper3 = median(salt_pepper_img, disk(1))
salt_pepper5 = median(salt_pepper_img, disk(2))
salt_pepper7 = median(salt_pepper_img, disk(3))

gaussian3 = median(gaussian_img, disk(1))
gaussian5 = median(gaussian_img, disk(2))
gaussian7 = median(gaussian_img, disk(3))

# =============================================================================
# plt.figure(figsize=(12, 8))
# plt.subplot(221)
# plt.imshow(gaussian_img)
# 
# plt.subplot(222)
# plt.imshow(gaussian3)
# 
# plt.subplot(223)
# plt.imshow(gaussian5)
# 
# plt.subplot(224)
# plt.imshow(gaussian7)
# =============================================================================

# =============================================================================
# plt.figure(figsize=(12, 8))
# plt.subplot(221)
# plt.imshow(salt_pepper_img)
# 
# plt.subplot(222)
# plt.imshow(salt_pepper3)
# 
# plt.subplot(223)
# plt.imshow(salt_pepper5)
# 
# plt.subplot(224)
# plt.imshow(salt_pepper7)
# =============================================================================

#imsave('filename.png', array)
