# Read in an image
from skimage.io import imread

# to calculate the local binary patterns & histograms
from skimage.feature import local_binary_pattern
from skimage.exposure import histogram, equalize_hist

# to calculate the euclidean distance
from scipy.spatial.distance import euclidean

def compare(filepath, comparison_path):
    image1 = imread(comparison_path, as_gray=True)
    image2 = imread(filepath, as_gray=True)
    
    lbp_image1 = local_binary_pattern(image1, 8, 1, method='uniform')
    lbp_image2 = local_binary_pattern(image2, 8, 1, method='uniform')
    
    hist1, bin1 = histogram(lbp_image1)
    hist2, bin2 = histogram(lbp_image2)
    
    eh1 = equalize_hist(hist1)
    eh2 = equalize_hist(hist2)
    
    distance = euclidean(eh1, eh2)
    
    if distance <= 0.006:
        result = 'YES'
    else:
        result = 'NO'
    
    return result
