from skimage.io import imread, imshow, imsave
from skimage.filters import roberts, sobel, prewitt
from skimage.feature import canny
from skimage.util import invert

image = imread('nan.jpg', as_gray=True)
#imshow(image)

roberts_edges = roberts(image)
canny_edges = canny(image)
sobel_edges = sobel(image)
prewitt_edges = prewitt(image)

r_neg = invert(roberts_edges)
c_neg = invert(canny_edges)
s_neg = invert(sobel_edges)
p_neg = invert(prewitt_edges)

imshow(canny_edges)
#imshow(c_neg)

imsave('roberts_nan.png', roberts_edges)
imsave('sobel_nan.png', sobel_edges)
imsave('prewitt_nan.png', prewitt_edges)

imsave('r_neg_nan.png', r_neg)
imsave('s_neg_nan.png', s_neg)
imsave('p_neg_nan.png', p_neg)
