# python version 3.7.1
# scikit-image version 0.14.2
# numpy version 1.15.4
# matplotlib version 3.0.2

# homework 2

# problem 3
# FFT
import numpy as np
import matplotlib.pyplot as plt
from skimage.io import imshow

# step 1
# create images

# single edge
edge = np.zeros((256,256))
edge[0:256, 128:256] = 1
#imshow(edge)

# a box
box = np.zeros((256,256))
box[78:178, 78:178] = 1
#imshow(box)

# step 2
# calculate the FFT
edge_fft = np.fft.fft2(edge)
box_fft = np.fft.fft2(box)

# shift DC coefficient to center
# "For two-dimensional input, swaps first and third quadrants, 
# and second and fourth quadrants.
edge_shift = np.fft.fftshift(edge)
box_shift = np.fft.fftshift(box)

# step 3
# display the transform of each image
plt.figure()
plt.imshow(edge_shift)
#plt.imshow(box_shift)

#t = np.arange(256)
#plt.plot(t, edge_shift.real, 'b-', t, edge_shift.imag, 'r--')
#plt.show()
#plt.plot(t, box_shift.real, 'b-', t, box_shift.imag, 'r--')
#plt.show()
