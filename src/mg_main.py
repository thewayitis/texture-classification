# TEXTURE MATCHING GAME MAIN
from mg_compare import compare
import random

# True while the game is still running
running = True

print("Texture Classification Experiment Running...")
print("<<Texture Matching Game>>")
print()

# set up this round of comparisons
# count the number of rounds (max 3 rounds)
guess_number = 1

comparison = random.randint(1,5)
    
if comparison == 1:
    comparison_path = 'canvas1/canvas1-a-p001.png'
elif comparison == 2:
    comparison_path = 'cushion1/cushion1-a-p001.png'
elif comparison == 3:
   comparison_path = 'linsseeds1/linseeds1-a-p001.png'
elif comparison == 4:
   comparison_path = 'sand1/sand1-a-p001.png'
elif comparison == 5:
   comparison_path = 'stone1/stone1-a-p001.png'

while(running):

    print("1. Canvas, 2. Cushion, 3. Linseed, 4. Sand, 5. Stone")
    texture = input("Enter a number to guess the texture, or Q to exit: ")
    
    if texture == '1':
        result = compare('canvas1/canvas1-a-p040.png', comparison_path)
    elif texture == '2':
        result = compare('cushion1/cushion1-a-p035.png', comparison_path)
    elif texture == '3':
        result = compare('linsseeds1/linseeds1-a-p037.png', comparison_path)
    elif texture == '4':
        result = compare('sand1/sand1-a-p026.png', comparison_path)
    elif texture == '5':
        result = compare('stone1/stone1-a-p012.png', comparison_path)
    elif texture == 'Q':
        running = False
        result = "n/a"
    else:
        result = "Please enter a number between 1 and 5, or Q to quit."
      
    print()
    print("Texture match?: ", result)
    print()
    
    if (result == 'NO') and (guess_number < 3):
        guess_number+=1
        print("Sorry, you've guessed incorrectly!")
        print()
    elif (result == 'YES') and (guess_number == 1):
        print("Wow!! You got it in the first guess!")
        print()
        running = False
    elif result == 'YES' and (guess_number <= 3):
        print("You got it! Congrats!")
        print()
        running = False
    elif guess_number >= 3:
        print("Sorry! The game is over.")
        print()
        running = False
    else:
        pass
        
