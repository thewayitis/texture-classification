# python version 3.7.1
# numpy version 1.15.4
# scikit-image version 0.14.2
# numpy version 1.15.4

from skimage.io import imread
import numpy as np
import matplotlib.pyplot as plt

image1 = imread('lena.JPG', as_gray=True)
image2 = imread('iris-illustration.BMP', as_gray=True)

one_fft = np.fft.fft2(image1)
two_fft = np.fft.fft2(image2)

fft1 = np.fft.fftshift(one_fft)
fft2 = np.fft.fftshift(two_fft)

#plt.imshow(np.log10(abs(fft1)))
#plt.imshow(np.log10(abs(fft2)))

d1 = imread('2pD.JPG', as_gray=True)
d2 = imread('2pD-2.JPG', as_gray=True)

d1_fft = np.fft.fft2(d1)
d1_shift = np.fft.fftshift(d1_fft)
d2_fft = np.fft.fft2(d2)
d2_shift = np.fft.fftshift(d2_fft)

#plt.imshow(np.log10(abs(d1_shift)))
plt.imshow(np.log10(abs(d2_shift)))
