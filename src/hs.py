import cv2
import matplotlib.pyplot as plt

# =============================================================================
# the image
# =============================================================================

# read in the image
img = cv2.imread('catPC.jpg', 0)

# uncomment to display the image
#cv2.imshow("image", img)

# program will continue after closing "image"
#cv2.waitKey(0)
#cv2.destroyAllWindows() 

# =============================================================================
# isplay the histogram, identify peaks with the "objects" they correspond to
# =============================================================================

# create the histogram and modify axis labels
plt.hist(img.flatten(), 256, [0, 256])
plt.xlim([0, 256])
plt.ylim([0, 20000])

# display the histogram
plt.show()


# =============================================================================
# show the identified object as binary images for each range
# =============================================================================

# 0 - 13
img = cv2.imread('cat.png', 0)

# threshold: image, threshold value, max value, threshold type
# with INV, all values below 13 are "white"
output_array, image1 = cv2.threshold(img, 13, 225, cv2.THRESH_BINARY_INV)

# the image is yellow and magenta...????
plt.imsave('th1.png', image1)

# 0 - 60
img = cv2.imread('cat.png', 0)

# threshold: image, threshold value, max value, threshold type
# with INV, all values below 60 are "white"
output_array, image2 = cv2.threshold(img, 60, 255, cv2.THRESH_BINARY_INV)

# the image is yellow and magenta...????
plt.imsave('th2.png', image2)

# 0 - 125
img = cv2.imread('cat.png', 0)

# threshold: image, threshold value, max value, threshold type
# with INV, all values below 125 are "white"
output_array, image3 = cv2.threshold(img, 125, 255, cv2.THRESH_BINARY_INV)

# the image is yellow and magenta...????
plt.imsave('th3.png', image3)

# 0 - 210
img = cv2.imread('cat.png', 0)

# threshold: image, threshold value, max value, threshold type
# with INV, all values below 210 are "white"
output_array, image4 = cv2.threshold(img, 210, 255, cv2.THRESH_BINARY_INV)

# the image is yellow and magenta...????
plt.imsave('th4.png', image4)

# 0 - 230
img = cv2.imread('cat.png', 0)

# threshold: image, threshold value, max value, threshold type
# with INV, all values below 230 are "white"
output_array, image5 = cv2.threshold(img, 230, 255, cv2.THRESH_BINARY_INV)

# the image is yellow and magenta...????
plt.imsave('th5.png', image5)

# 0 - 250
img = cv2.imread('cat.png', 0)

# threshold: image, threshold value, max value, threshold type
# with INV, all values below 250 are "white"
output_array, image6 = cv2.threshold(img, 250, 255, cv2.THRESH_BINARY_INV)

# the image is yellow and magenta...????
plt.imsave('th6.png', image6)

# =============================================================================
# construct the histogram-based segmented image, by combining the binary images
# =============================================================================

segmented_image = image5 + image4 + image3 + image2 + image1
cv2.imwrite('segments_combined.png', segmented_image)
