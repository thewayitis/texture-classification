# python version 3.7.1
# scikit-image version 0.14.2
# numpy version 1.15.4

# homework 2

# problem 1
# template matching and multi-resolution image representation:
# perform a sample template matching using the multi-correlation 
# approach
import time
import numpy as np
from skimage.io import imread#, imsave
from skimage.filters import gaussian
from skimage.transform import resize
from skimage.feature import match_template

# step 1
# reduce resolution of both template and image by creating
# an image pyramid

# import each image and template
# image LEVEL 0
i0 = imread('image.JPG', as_gray=True)
# i0 shape is (189, 441)

# template LEVEL 0
t0 = imread('template.JPG', as_gray=True)
# t0 shape is (86, 55)

# use gaussian filter to blur
i0_blur = gaussian(i0)
t0_blur = gaussian(t0)

# use method to downscale  LEVEL 1
i1 = resize(i0_blur, (round(i0.shape[0]/2), round(i0.shape[1]/2)))
t1 = resize(t0_blur, (round(t0.shape[0]/2), round(t0.shape[1]/2)))

# continue making the pyramid
i1_blur = gaussian(i1)
t1_blur = gaussian(t1)

#  LEVEL 2
i2 = resize(i1_blur, (round(i1.shape[0]/2), round(i1.shape[1]/2)))
t2 = resize(t1_blur, (round(t1.shape[0]/2), round(t1.shape[1]/2)))
i2_blur = gaussian(i2)
t2_blur = gaussian(t2)

#  LEVEL 3
i3 = resize(i2_blur, (round(i2.shape[0]/2), round(i2.shape[1]/2)))
t3 = resize(t2_blur, (round(t2.shape[0]/2), round(t2.shape[1]/2)))
i3_blur = gaussian(i3)
t3_blur = gaussian(t3)

#  LEVEL 4
i4 = resize(i3_blur, (round(i3.shape[0]/2), round(i3.shape[1]/2)))
t4 = resize(t3_blur, (round(t3.shape[0]/2), round(t3.shape[1]/2)))
i4_blur = gaussian(i4)
t4_blur = gaussian(t4)

#  LEVEL 5
i5 = resize(i4_blur, (round(i4.shape[0]/2), round(i4.shape[1]/2)))
t5 = resize(t4_blur, (round(t4.shape[0]/2), round(t4.shape[1]/2)))

# step 2
# match small template against small image (level 5)
start = time.time()
match5 = match_template(i5, t5)
end = time.time()

# step 3
# identify location of strong matches / determine coordinates
ij = np.unravel_index(np.argmax(match5), match5.shape)
x, y = ij[::-1]
print("Level 5 coordinates:")
print(x)
print(y)
print("Search Time:")
print(end-start)
print()
# i5 has 6 rows, 14 columns
# location of template match at 1st row, 6th column

# step 4
# expand the image and template and match higher resolution template
# selectively to higher resolution image

# i4 has 12 rows, 28 columns
# location of template match at 2nd row, 10th column
start = time.time()
match4 = match_template(i4, t4)
end = time.time()

ij = np.unravel_index(np.argmax(match4), match4.shape)
x, y = ij[::-1]
print("Level 4 coordinates:")
print(x)
print(y)
print("Search Time:")
print(end-start)
print()

# step 5
# iterate on higher and higher resolution images
start = time.time()
match3 = match_template(i3, t3)
end = time.time()

ij = np.unravel_index(np.argmax(match3), match3.shape)
x, y = ij[::-1]
print("Level 3 coordinates:")
print(x)
print(y)
print("Search Time:")
print(end-start)
print()

start = time.time()
match2 = match_template(i2, t2)
end = time.time()

ij = np.unravel_index(np.argmax(match2), match2.shape)
x, y = ij[::-1]
print(x)
print(y)
print("Search Time:")
print(end-start)
print()

start = time.time()
match1 = match_template(i1, t1)
end = time.time()

ij = np.unravel_index(np.argmax(match1), match1.shape)
x, y = ij[::-1]
print("Level 2 coordinates:")
print(x)
print(y)
print("Search Time:")
print(end-start)
print()

start = time.time()
match0 = match_template(i0, t0)
end = time.time()

ij = np.unravel_index(np.argmax(match0), match0.shape)
x, y = ij[::-1]
print("Level 1 coordinates:")
print(x)
print(y)
print("Search Time:")
print(end-start)
print()

#imsave('i2.PNG', i2)
