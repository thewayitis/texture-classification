
from PIL import Image

# write a computer program capable of reducing the number of gray levels 
# in an image from 256 to 2, in integer powers of 2. The desired number
# of gray levels is an input to the program.

def grayLevels(inputImage, levels):
    "Enter the input image file path and the requested number of \
    gray levels 2,4,8,16,32,64,128,256 (Ex. 'eye.png', 128)"
    
    # read in the image
    myImage = Image.open(inputImage, mode='r')
    
    # convert the image to the number of levels given by the user
    newImage = myImage.convert("P", palette=Image.ADAPTIVE, colors=levels)

    # save image to file / display the image
    newImage.save('gray' + str(levels) + '.bmp')
    newImage.show()


grayLevels('eye.png', 2)
