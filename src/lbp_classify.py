# Read in an image, calculate local binary pattern, 
# create and compare histograms
from skimage.io import imread

# to calculate the local binary patterns & histograms
from skimage.feature import local_binary_pattern
from skimage.exposure import histogram, equalize_hist

import numpy as np
# support vector machine
from sklearn.svm import SVC

# 200 training images (40 for each texture type)
train_features = []
train_labels = np.array([
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
    1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 
    4, 4, 4, 4, 4])

texture_names = ['Canvas', 'Cushion', 'Linseed', 'Sand', 'Stone']

# extract LBP features for training

# canvas
canvas_path = 'canvas1-without-rotations/'
canvas_below_10 = 'canvas1-a-p00'
canvas_10_above = 'canvas1-a-p0'

# test the predictions for canvas
# canvas_path
canvas_rotation = 'canvas1-with-rotations/'
canvas_test_path = 'canvas1-b-p0'
canvas_test_features = []

# cushion
cushion_path = 'cushion1-without-rotations/'
cushion_below_10 = 'cushion1-a-p00'
cushion_10_above = 'cushion1-a-p0'

# test the predictions for cushion
# cushion_path
cushion_rotation = 'cushion1-with-rotations/'
cushion_test_path = 'cushion1-b-p0'
cushion_test_features = []

# linseed
linseed_path = 'linseeds1-without-rotations/'
linseed_below_10 = 'linseeds1-a-p00'
linseed_10_above = 'linseeds1-a-p0'

# test the predictions for linseed
# linseed_path
linseed_rotation = 'linseeds1-with-rotations/'
linseed_test_path = 'linseeds1-b-p0'
linseed_test_features = []

# sand
sand_path = 'sand1-without-rotations/'
sand_below_10 = 'sand1-a-p00'
sand_10_above = 'sand1-a-p0'

# test the predictions for sand
# sand_path
sand_rotation = 'sand1-with-rotations/'
sand_test_path = 'sand1-b-p0'
sand_test_features = []

# stone
stone_path = 'stone1-without-rotations/'
stone_below_10 = 'stone1-a-p00'
stone_10_above = 'stone1-a-p0'

# test the predictions for sand
# stone_path
stone_rotation = 'stone1-with-rotations/'
stone_test_path = 'stone1-b-p0'
stone_test_features = []

# canvas
for i in range(1,10):
    # load the texture image, extract lbp feature and add to training features list
    image_path = canvas_path + canvas_below_10 + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

for i in range(10,41):
    # load the texture image, extract lbp feature and add to training features list
    image_path = canvas_path + canvas_10_above + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

# cushion
for i in range(1,10):
    # load the texture image, extract lbp feature and add to training features list
    image_path = cushion_path + cushion_below_10 + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

for i in range(10,41):
    # load the texture image, extract lbp feature and add to training features list
    image_path = cushion_path + cushion_10_above + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

# linseed
for i in range(1,10):
    # load the texture image, extract lbp feature and add to training features list
    image_path = linseed_path + linseed_below_10 + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

for i in range(10,41):
    # load the texture image, extract lbp feature and add to training features list
    image_path = linseed_path + linseed_10_above + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

# sand
for i in range(1,10):
    # load the texture image, extract lbp feature and add to training features list
    image_path = sand_path + sand_below_10 + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

for i in range(10,41):
    # load the texture image, extract lbp feature and add to training features list
    image_path = sand_path + sand_10_above + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

# stone
for i in range(1,10):
    # load the texture image, extract lbp feature and add to training features list
    image_path = stone_path + stone_below_10 + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)

for i in range(10,41):
    # load the texture image, extract lbp feature and add to training features list
    image_path = stone_path + stone_10_above + str(i) + '.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    train_features.append(equalized_hist)    

print("Training features: {}".format(np.array(train_features).shape))
print("Training labels: {}".format(train_labels.shape))

# create and train the SVM
svm_classifier = SVC(gamma='auto')
svm_classifier.fit(train_features, train_labels)

# test the predictions for canvas
for i in range(11, 41):
    # load the texture image and add to testing list
    image_path = canvas_path + canvas_test_path + str(i) + '.png'
    #image_path = canvas_rotation + canvas_10_above + str(i) + '-r060.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    canvas_test_features.append(equalized_hist)

prediction = svm_classifier.predict(canvas_test_features)

print(texture_names[0], "Prediction: ", prediction)

# test the predictions for cushion
for i in range(11, 41):
    # load the texture image and add to testing list
    image_path = cushion_path + cushion_test_path + str(i) + '.png'
    #image_path = cushion_rotation + cushion_10_above + str(i) + '-r060.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    cushion_test_features.append(equalized_hist)

prediction = svm_classifier.predict(cushion_test_features)

print(texture_names[1], "Prediction: ", prediction)

# test the predictions for linseed
for i in range(11, 41):
    # load the texture image and add to testing list
    image_path = linseed_path + linseed_test_path + str(i) + '.png'
    #image_path = linseed_rotation + linseed_10_above + str(i) + '-r060.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    linseed_test_features.append(equalized_hist)

prediction = svm_classifier.predict(linseed_test_features)

print(texture_names[2], "Prediction: ", prediction)

# test the predictions for sand
for i in range(11, 41):
    # load the texture image and add to testing list
    image_path = sand_path + sand_test_path + str(i) + '.png'
    #image_path = sand_rotation + sand_10_above + str(i) + '-r060.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    sand_test_features.append(equalized_hist)

prediction = svm_classifier.predict(sand_test_features)

print(texture_names[3], "Prediction: ", prediction)

# test the predictions for stone
for i in range(11, 41):
    # load the texture image and add to testing list
    image_path = stone_path + stone_test_path + str(i) + '.png'
    #image_path = stone_rotation + stone_10_above + str(i) + '-r060.png'
    texture_image = imread(image_path, as_gray=True)
    texture_lbp = local_binary_pattern(texture_image, 8, 1, method='uniform')
    texture_hist, texture_bins = histogram(texture_lbp)
    equalized_hist = equalize_hist(texture_hist)
    stone_test_features.append(equalized_hist)

prediction = svm_classifier.predict(stone_test_features)

print(texture_names[4], "Prediction: ", prediction)

# score the testing feature predictions
canvas_test_labels = np.array([0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0])
canvas_score = svm_classifier.score(canvas_test_features, canvas_test_labels)
print("Canvas Score: ", canvas_score)

cushion_test_labels = np.array([1,1,1,1,1,1,1,1,1,1,
1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1])
cushion_score = svm_classifier.score(cushion_test_features, cushion_test_labels)
print("Cushion Score: ", cushion_score)

linseed_test_labels = np.array([2,2,2,2,2,2,2,2,2,2,
2,2,2,2,2,2,2,2,2,2, 2,2,2,2,2,2,2,2,2,2])
linseed_score = svm_classifier.score(linseed_test_features, linseed_test_labels)
print("Linseed Score: ", linseed_score)

sand_test_labels = np.array([3,3,3,3,3,3,3,3,3,3,
3,3,3,3,3,3,3,3,3,3, 3,3,3,3,3,3,3,3,3,3])
sand_score = svm_classifier.score(sand_test_features, sand_test_labels)
print("Sand Score: ", sand_score)

stone_test_labels = np.array([4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4, 4,4,4,4,4,4,4,4,4,4])
stone_score = svm_classifier.score(stone_test_features, stone_test_labels)
print("Stone Score: ", stone_score)
