# python version 3.7.1
# scikit-image version 0.14.2

# Assignment 3
# problem 1
from skimage.io import imread, imshow
from skimage.feature import canny

# open the image and display it
house = imread('house.JPG', as_gray=True)
#imshow(house)

# adjust canny input parameters
canny_house = canny(house, sigma=2., \
                    low_threshold=0.10, high_threshold=0.10)
imshow(canny_house)


# canny(image, sigma=1.0, low_threshold=None, 
# high_threshold=None, mask=None, use_quantiles=False)

# sigma represents the sigma deviation of the Gaussian filter
# hysteresis thresholding (linking edges)
