import matplotlib.image as image
import matplotlib.pyplot as plt

# write a computer program that takes an input image and a desired change value
def interpolate(inputImage, rescale):
    "Enter an input image file path and the desired spatial resolution change (Ex. 'eye.png', 0.5)"
    
    # "thumbnail" method to resize the image, use nearest, bilinear, and bicubic
    image.thumbnail(inputImage, 'near.png', scale=rescale, interpolation='nearest', preview=False)
    image.thumbnail(inputImage, 'bil.png', scale=rescale, interpolation='bilinear', preview=False)
    image.thumbnail(inputImage, 'bic.png', scale=rescale, interpolation='bicubic', preview=False)
    
    # then return the image to its original resolution
    
    # if the number is greater than one, flip the rescale number (newscale = 1/rescale)
    # if the number is less than one, use the inflation formula inflation = 100*rescale, newscale = 100/inflation
    if (rescale < 1):
        
        # determine the new scale
        inflation = 100 * rescale
        newscale = 100/inflation
        
        # transform images
        image.thumbnail('near.png', 'nearest.png', scale=newscale, interpolation='none', preview=False)
        image.thumbnail('bil.png', 'bilinear.png', scale=newscale, interpolation='none', preview=False)
        image.thumbnail('bic.png', 'bicubic.png', scale=newscale, interpolation='none', preview=False)
        
    else:
        
        # determine the new scale
        newscale = 1 / rescale
        
        # transform images
        image.thumbnail('near.png', 'nearest.png', scale=newscale, interpolation='none', preview=False)
        image.thumbnail('bil.png', 'bilinear.png', scale=newscale, interpolation='none', preview=False)
        image.thumbnail('bic.png', 'bicubic.png', scale=newscale, interpolation='none', preview=False)
    
    
    # change the image being read to any of the following:
    
    # 'near.png', 'bil.png', 'bic.png' for the rescaled image 
    # (scale given by user) or 'nearest.png', 'bilinear.png', 
    # 'bicubic.png' for the image returned to the original 
    # resolution, post interpolation method.
    
    myImage = plt.imread('nearest.png')
    
    # display the image
    plt.imshow(myImage)

    
interpolate('eye.png', 0.5)
