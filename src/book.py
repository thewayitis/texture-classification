# =============================================================================
# from PIL import Image, ImageFilter
# 
# img = Image.open('filter.tif')
# 
# median = img.filter(ImageFilter.MedianFilter(3))
# median.save('median3.png')
# median = img.filter(ImageFilter.MedianFilter(7))
# median.save('median7.png')
# median = img.filter(ImageFilter.MedianFilter(9))
# median.save('median9.png')
# =============================================================================

# from https://opencv-python-tutroals.readthedocs.io/en/latest/
# py_tutorials/py_imgproc/py_filtering/py_filtering.html

import cv2
import numpy as np
import matplotlib.pyplot as plt

image = cv2.imread('filter.tif')

three = np.ones((3,3), np.float32)/9
seven = np.ones((7,7), np.float32)/49
nine = np.ones((9,9), np.float32)/81

image_three = cv2.filter2D(image, -1, three)
plt.imshow(image_three)
plt.show()

image_seven = cv2.filter2D(image, -1, seven)
plt.imshow(image_seven)
plt.show()

image_nine = cv2.filter2D(image, -1, nine)
plt.imshow(image_nine)
plt.show()

#cv2.imwrite('image_three.png', image_three)  
#cv2.imwrite('image_seven.png', image_seven)  
#cv2.imwrite('image_nine.png', image_nine)  
