import matplotlib.pyplot as plt
import cv2
import numpy as np
from skimage.util import invert
from skimage.filters import threshold_otsu
# a)
# load image as grayscale
image_gray = cv2.imread('balloons.jpg', 0)
#cv2.imwrite('gray_balloons.png', image_gray)
plt.imshow(image_gray)
plt.show()

# histogram for grayscale image
plt.hist(image_gray.flatten(), 256, [0, 256])
plt.xlim([0, 256])
plt.ylim([0,3000])
plt.show()

# openCV reads in as BGR format
# original image
image = cv2.imread('balloons.jpg')
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
plt.imshow(image)
plt.show()

# a) replace the backgroung pixels
image[np.where((image == [246, 246, 246]).all(axis = 2))] = [0, 0, 0]
#image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
#cv2.imwrite('black_balloons.png', image)       
plt.imshow(image)
plt.show()

# write about success and failure

# b)
imageB = cv2.imread('balloons.jpg')
imageB = cv2.cvtColor(imageB, cv2.COLOR_BGR2RGB)
plt.imshow(imageB)
plt.show()

B, G, R = cv2.split(imageB)

#cv2.imwrite('red_balloons.png', R)  
#cv2.imwrite('green_balloons.png', G) 
#cv2.imwrite('blue_balloons.png', B) 

plt.imshow(R)
plt.show()

plt.imshow(G)
plt.show()
 
plt.imshow(B)
plt.show()

r_neg = invert(R)
g_neg = invert(G)
b_neg = invert(B)

plt.imshow(r_neg)
plt.show()

plt.imshow(g_neg)
plt.show()
 
plt.imshow(b_neg)
plt.show()

#cv2.imwrite('neg_red.png', r_neg)
#cv2.imwrite('neg_green.png', g_neg)
#cv2.imwrite('neg_blue.png', b_neg)

plt.hist(R.flatten(), 256, [0, 256])
plt.xlim([0, 256])
plt.ylim([0,2400])
plt.show()

rv_r, r_thr45 = cv2.threshold(R, 45, 255, cv2.THRESH_BINARY)
plt.imshow(r_thr45)
plt.show()

rv_r, r_thr100 = cv2.threshold(R, 100, 255, cv2.THRESH_BINARY)
plt.imshow(r_thr100)
plt.show()

plt.hist(G.flatten(), 256, [0, 256])
plt.xlim([0, 256])
plt.ylim([0,2800])
plt.show()

rv_g,g_thr = cv2.threshold(G, 190, 255, cv2.THRESH_BINARY)
plt.imshow(g_thr)
plt.show()

plt.hist(B.flatten(), 256, [0, 256])
plt.xlim([0, 256])
plt.ylim([0,5000])
plt.show()

rv_b, b_thr = cv2.threshold(B, 200, 255, cv2.THRESH_BINARY)
plt.imshow(b_thr)
plt.show()

#cv2.imwrite('r_thr100.png', r_thr100) 
#cv2.imwrite('g_thr.png', g_thr) 
#cv2.imwrite('b_thr.png', b_thr) 

# 481 students, automatic thresholding with Otsu
rv_R, r_threshold = cv2.threshold(R, 0, 255, cv2.THRESH_OTSU)
plt.imshow(r_threshold)
plt.show()

rv_G, g_threshold = cv2.threshold(G, 0, 255, cv2.THRESH_OTSU)
plt.imshow(g_threshold)
plt.show()

rv_B, b_threshold = cv2.threshold(B, 0, 255, cv2.THRESH_OTSU)
plt.imshow(b_threshold)
plt.show()


#cv2.imwrite('r_threshold.png', r_threshold) 
#cv2.imwrite('g_threshold.png', g_threshold) 
#cv2.imwrite('b_threshold.png', b_threshold) 


# c) HSI space thresholds
imageC = cv2.imread('balloons.jpg')
imageC = cv2.cvtColor(imageC, cv2.COLOR_BGR2RGB)

#image_hsv = cv2.cvtColor(imageC, cv2.COLOR_RGB2HSV)
image_hsv = cv2.cvtColor(imageC, cv2.COLOR_RGB2HLS)
plt.imshow(image_hsv)
plt.show()
#cv2.imwrite('image_hsv_actual.png', image_hsv) 

h, s, v = cv2.split(image_hsv)

rv_h, h_threshold = cv2.threshold(h, 0, 255, cv2.THRESH_OTSU)
plt.imshow(h_threshold)
plt.show()

rv_s, s_threshold = cv2.threshold(s, 0, 255, cv2.THRESH_OTSU)
plt.imshow(s_threshold)
plt.show()

rv_v, v_threshold = cv2.threshold(v, 0, 255, cv2.THRESH_OTSU)
plt.imshow(v_threshold)
plt.show()

#cv2.imwrite('h_threshold.png', h_threshold) 
#cv2.imwrite('s_threshold.png', s_threshold) 
#cv2.imwrite('v_threshold.png', v_threshold) 

val = threshold_otsu(v)
print(val)
return_val, threshold_image = cv2.threshold(v, val, 255, cv2.THRESH_BINARY)
plt.imshow(threshold_image)
plt.show()

#cv2.imwrite('threshold.png', threshold_image) 

combined = r_threshold & g_threshold & b_threshold
plt.imshow(combined)
plt.show()

#cv2.imwrite('combined.png', combined)
