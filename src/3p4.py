from math import sqrt

haar = [
        [1, 1, 1, 1],
        [1, 1, -1, -1],
        [sqrt(2), -1*(sqrt(2)), 0, 0],
        [0, 0, sqrt(2), -1*(sqrt(2))]
        ]

haar_t = [
        [1, 1, sqrt(2), 0],
        [1, 1, -1*(sqrt(2)), 0],
        [1, -1, 0, sqrt(2)],
        [1, -1, 0, -1*(sqrt(2))]
        ]

# based off example 6.19
def haar_transform(x):
    total = 0
    print("x:")
    print(x)
    print()

    # for each row in haar
    for i in range(len(haar)):
        # for each column in haar
        for j in range(len(haar[0])):
            # multiply the matrices
            temp = haar[i][j] * x[j]
            # sum the values
            total += temp
    
        # multiply by one half and reset total count
        row_total = total / 2
        total = 0
        print("Result Row", i, ":")
        print(row_total)

haar_transform([1,4,-3,0])
print()

# a 
def haar(x):
    # addition
    addition = []
    for i in range(len(x)):
        if i%2 == 0:
            temp = x[i] + x[i+1]
            #print(temp)
            addition.append(temp)
    # add the numbers and subtract the numbers once more
    # for the addition array only
    sum_diff = []
    for i in range(len(addition)):
        if i%2 == 0:
            plus = addition[i] + addition[i+1]
            minus = addition[i] - addition[i+1]
           
            sum_diff.append(plus)
            sum_diff.append(minus)
        
    # divide the addition numbers by sqrt(2)
    #print(sum_diff)
    for i in range(len(sum_diff)):
        sum_diff[i] = sum_diff[i] / sqrt(2)
    #print(sum_diff)
    
    # subtraction        
    for i in range(len(x)):
        if i%2 == 0:
            temp = x[i] - x[i+1]
            sum_diff.append(temp)
    #print(sum_diff)
    # divide all numbers by sqrt(2)
    for i in range(len(sum_diff)):
        sum_diff[i] = sum_diff[i] / sqrt(2)
    
    print("a) result:")
    print("x:")
    print(x)
    print()
    print(sum_diff)
    print()
    return sum_diff
    
result = haar([1,4,-3,0])
#result = haar([0,1,2,3])

# b
def inv_haar(x):
    total = 0
    print("b) result:")
    print("x:")
    print(x)
    print()

    # for each row in haar transposed
    for i in range(len(haar_t)):
        # for each column in haar
        for j in range(len(haar_t[0])):
            # multiply the matrices
            temp = haar_t[i][j] * x[j]
            # sum the values
            total += temp
    
        # multiply by one half and reset total count
        row_total = total / 2
        total = 0
        print("Result Row", i, ":")
        print(row_total)

inv_haar(result)
