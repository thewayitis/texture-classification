from PIL import Image
from PIL import ImageOps

# (a) Read and display an image.
myImage = Image.open('catkitten.jpg', mode="r")
myImage.save('original.bmp')
# uncomment this line to show the original image
#myImage.show()

# (b) Calculate the negative of the image and display it.
# s = L - 1 - r, where r is the intensity value to be transformed
# 'Invert' method negates the image.
negativeImage = ImageOps.invert(myImage)
negativeImage.save('negative.bmp')
# uncomment this line to show the negative image
#negativeImage.show()

# (c) Perform contrast stretching where (r1, s1) = (r-min, 0) 
# and (r2, s2) = (r-max, L-1). 'Autocontrast' method remaps the image 
# so that the darkest pixel becomes black (0) and the lightest 
# pixel becomes white (255/1)
stretchImage = ImageOps.autocontrast(negativeImage, cutoff=20)
stretchImage.save('stretch2.bmp')
#stretchImage.show()
