# python version 3.7.1
# scikit-image version 0.14.2
# numpy version 1.15.4

# homework 2

# problem 2
# blend two images using Laplacian Pyramid approach
from skimage.io import imread, imsave, imshow
from skimage.filters import gaussian
from skimage.transform import resize
import numpy as np

# step 1
# build Laplacian Pyramid for image A
c0 = imread('cat.JPG', as_gray=True)

# build gaussian pyramid
c0_blur = gaussian(c0, multichannel=False)
c1 = resize(c0_blur, (round(c0.shape[0]/2), round(c0.shape[1]/2)))
c1_blur = gaussian(c1, multichannel=False)
c2 = resize(c1_blur, (round(c1.shape[0]/2), round(c1.shape[1]/2)))
c2_blur = gaussian(c2, multichannel=False)
c3 = resize(c2_blur, (round(c2.shape[0]/2), round(c2.shape[1]/2)))
c3_blur = gaussian(c3, multichannel=False)
c4 = resize(c3_blur, (round(c3.shape[0]/2), round(c3.shape[1]/2)))
c4_blur = gaussian(c4, multichannel=False)
c5 = resize(c4_blur, (round(c4.shape[0]/2), round(c4.shape[1]/2)))

# build laplacian pyramid
c_lap0 = c0 - c0_blur
c_lap1 = c1 - c1_blur
c_lap2 = c2 - c2_blur
c_lap3 = c3 - c3_blur
c_lap4 = c4 - c4_blur
c_lap5 = c5

# step 2
# build Laplacian Pyramid for image B
d0 = imread('dog.JPG', as_gray=True)

# build gaussian pyramid
d0_blur = gaussian(d0, multichannel=False)
d1 = resize(d0_blur, (round(d0.shape[0]/2), round(d0.shape[1]/2)))
d1_blur = gaussian(d1, multichannel=False)
d2 = resize(d1_blur, (round(d1.shape[0]/2), round(d1.shape[1]/2)))
d2_blur = gaussian(d2, multichannel=False)
d3 = resize(d2_blur, (round(d2.shape[0]/2), round(d2.shape[1]/2)))
d3_blur = gaussian(d3, multichannel=False)
d4 = resize(d3_blur, (round(d3.shape[0]/2), round(d3.shape[1]/2)))
d4_blur = gaussian(d4, multichannel=False)
d5 = resize(d4_blur, (round(d4.shape[0]/2), round(d4.shape[1]/2)))

# build laplacian pyramid
d_lap0 = d0 - d0_blur
d_lap1 = d1 - d1_blur
d_lap2 = d2 - d2_blur
d_lap3 = d3 - d3_blur
d_lap4 = d4 - d4_blur
d_lap5 = d5

# step 3
# create third Laplacian Pyramid
# combine images
combined_image_cd0 = np.zeros(c0.shape)
height = c0.shape[0]
width = c0.shape[1]
mid_point = width // 2

for row in range(height):
    for col in range(width):
        if col < mid_point:
            combined_image_cd0[row, col] = c0[row, col]
        elif col > mid_point:
            combined_image_cd0[row, col] = d0[row, col]
        else:
            combined_image_cd0[row, col] = (c0[row, col] + d0[row, col]) / 2
            

combined_image_cd2 = np.zeros(c2.shape)
height = c2.shape[0]
width = c2.shape[1]
mid_point = width // 2
for row in range(height):
    for col in range(width):
        if col < mid_point:
            combined_image_cd2[row, col] = c2[row, col]
        elif col > mid_point:
            combined_image_cd2[row, col] = d2[row, col]
        else:
            combined_image_cd2[row, col] = (c2[row, col] + d2[row, col]) / 2
            

combined_image_cdlap0 = np.zeros(c0.shape)
height = c0.shape[0]
width = c0.shape[1]
mid_point = width // 2
for row in range(height):
    for col in range(width):
        if col < mid_point:
            combined_image_cdlap0[row, col] = c_lap0[row, col]
        elif col > mid_point:
            combined_image_cdlap0[row, col] = d_lap0[row, col]
        else:
            combined_image_cdlap0[row, col] = (c_lap0[row, col] + d_lap0[row, col]) / 2
            

combined_image_cdlap1 = np.zeros(c1.shape)
height = c1.shape[0]
width = c1.shape[1]
mid_point = width // 2
for row in range(height):
    for col in range(width):
        if col < mid_point:
            combined_image_cdlap1[row, col] = c_lap1[row, col]
        elif col > mid_point:
            combined_image_cdlap1[row, col] = d_lap1[row, col]
        else:
            combined_image_cdlap1[row, col] = (c_lap1[row, col] + d_lap1[row, col]) / 2
            

combined_image_cdlap2 = np.zeros(c2.shape)
height = c2.shape[0]
width = c2.shape[1]
mid_point = width // 2
for row in range(height):
    for col in range(width):
        if col < mid_point:
            combined_image_cdlap2[row, col] = c_lap2[row, col]
        elif col > mid_point:
            combined_image_cdlap2[row, col] = d_lap2[row, col]
        else:
            combined_image_cdlap2[row, col] = (c_lap2[row, col] + d_lap2[row, col]) / 2
            

combined_image_cdlap3 = np.zeros(c3.shape)
height = c3.shape[0]
width = c3.shape[1]
mid_point = width // 2
for row in range(height):
    for col in range(width):
        if col < mid_point:
            combined_image_cdlap3[row, col] = c_lap3[row, col]
        elif col > mid_point:
            combined_image_cdlap3[row, col] = d_lap3[row, col]
        else:
            combined_image_cdlap3[row, col] = (c_lap3[row, col] + d_lap3[row, col]) / 2
            

combined_image_cdlap4 = np.zeros(c4.shape)
height = c4.shape[0]
width = c4.shape[1]
mid_point = width // 2
for row in range(height):
    for col in range(width):
        if col < mid_point:
            combined_image_cdlap4[row, col] = c_lap4[row, col]
        elif col > mid_point:
            combined_image_cdlap4[row, col] = d_lap4[row, col]
        else:
            combined_image_cdlap4[row, col] = (c_lap4[row, col] + d_lap4[row, col]) / 2
            

combined_image_cdlap5 = np.zeros(c5.shape)
height = c5.shape[0]
width = c5.shape[1]
mid_point = width // 2
for row in range(height):
    for col in range(width):
        if col < mid_point:
            combined_image_cdlap5[row, col] = c_lap5[row, col]
        elif col > mid_point:
            combined_image_cdlap5[row, col] = d_lap5[row, col]
        else:
            combined_image_cdlap5[row, col] = (c_lap5[row, col] + d_lap5[row, col]) / 2
            
#imshow(combined_image_cdlap5)
