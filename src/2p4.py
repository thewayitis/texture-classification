# python version 3.7.1
# scikit-image version 0.14.2
# numpy version 1.15.4
# matplotlib version 3.0.2

# homework 2

# problem 4
# Gaussian filtering in the frequency domain
from skimage.io import imread
import numpy as np
import matplotlib.pyplot as plt

# create a gaussian filter
from skimage.filters import  gaussian

# multiply it by the FFT of the cameraman image
# read in image
cameraman = imread('cameraman.TIF', as_gray=True)
# compute FFT
cam_fft = np.fft.fft2(cameraman)
# multiply by filter
cam_G1 = gaussian(cam_fft.real, sigma=1)
cam_G10 = gaussian(cam_fft.real, sigma=10)
cam_G30 = gaussian(cam_fft.real, sigma=30)

# used to plot the frequency domains
t = np.arange(512)

plt.plot(t, cam_G1.real, 'b-', t, cam_G1.imag, 'r--')
plt.show()

plt.plot(t, cam_G10.real, 'b-', t, cam_G10.imag, 'r--')
plt.show()

plt.plot(t, cam_G30.real, 'b-', t, cam_G30.imag, 'r--')
plt.show()

# invert the result (ifft2.m)
invert1 = np.fft.ifft2(cam_G1)
invert10 = np.fft.ifft2(cam_G10)
invert30 = np.fft.ifft2(cam_G30)

# show the images in the frequency domain?
# from numpy documentation for ifft
plt.plot(t, invert1.real, 'b-', t, invert1.imag, 'r--')
plt.show()

plt.plot(t, invert10.real, 'b-', t, invert10.imag, 'r--')
plt.show()

plt.plot(t, invert30.real, 'b-', t, invert30.imag, 'r--')
plt.show()

# to depict a gaussian filter in the frequency domain
# from https://code.i-harness.com/en/q/1064ef9
def fspecial_gauss(size, sigma):

    """Function to mimic the 'fspecial' gaussian MATLAB function
    """

    x, y = np.mgrid[-size//2 + 1:size//2 + 1, -size//2 + 1:size//2 + 1]
    g = np.exp(-((x**2 + y**2)/(2.0*sigma**2)))
    return g/g.sum()

gauss1 = fspecial_gauss(512, 1)
plt.plot(gauss1)
plt.show()

gauss10 = fspecial_gauss(512, 10)
plt.plot(gauss10)
plt.show()

gauss30 = fspecial_gauss(512, 30)
plt.plot(gauss30)
plt.show()
