from PIL import Image

def pixelReplication(inputImage, resizeFactor):
    "Enter the image file path and resize factor. A negative resizeFactor \
    means shrink, a positive resizeFactor means expand \
    [Ex. pixelReplication('cat.jpg', -4)]"    
    
    # read in the image
    myImage = Image.open(inputImage, mode="r")

    # determine if we are expanding or shrinking
    if isinstance(resizeFactor, int):
        if resizeFactor < 0:
            #print("shrinking")
             # determine the new size for the image    
            width = myImage.width
            height = myImage.height
    
            width = int(width * 1/(-1 * resizeFactor))
            height = int(height * 1/(-1 * resizeFactor))
    
            # create a new image, resize method resample interpolation:
            # 0 for nearest, 4 for box, 2 for bilinear, 3 for bicubic
            
            newImage = myImage.resize((width, height), resample=0)
            newImage.save('shrunk' + str(resizeFactor) + '.bmp')
            newImage.show()
        
        elif resizeFactor > 0:
            #print("expanding")
            # determine the new size for the image    
            width = myImage.width
            height = myImage.height
    
            width = width * resizeFactor
            height = height * resizeFactor
            
            # create a new image, resize method resample interpolation:
            # 0 for nearest, 4 for box, 2 for bilinear, 3 for bicubic
            newImage = myImage.resize((width, height), resample=0)
            newImage.save('expanded' + str(resizeFactor) + '.bmp')
            newImage.show()
            
        else:
            print("No resizing factor.")
        
    else:
        print("Error. Please enter a positive or negative integer.")


#pixelReplication('catmature.jpg', -4)
pixelReplication('shrunk-4.bmp', 4)
