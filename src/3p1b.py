# python version 3.7.1
# scikit-image version 0.14.2

from skimage.io import imread
from skimage.feature import canny
from skimage.transform import hough_line, hough_line_peaks

# import matplotlib.pyplot as plt
# from matplotlib import cm
# import numpy as np

# create the canny edge image
image = imread('house.JPG', as_gray=True)
canny_image = canny(image, sigma=2., \
                    low_threshold=0.10, high_threshold=0.10)

# default hough_line theta = a vector of 180 angles evenly spaced from 
# -pi/2 to pi/2
hspace, angles, distances = hough_line(canny_image)
hspace_peaks, angles_peaks, distances_peaks = hough_line_peaks(hspace, angles,\
                                                               distances)
# to display the different images
# http://scikit-image.org/docs/dev/auto_examples/edges/plot_line_hough_transform.html
# =============================================================================
# fix, axes = plt.subplots(3, 1, figsize=(14,14)) # 14, 8 # 14, 7
# axes = axes.ravel()
# 
# axes[0].imshow(canny_image, cmap=plt.cm.gray)
# axes[0].set_title('Input Image')
# 
# axes[1].imshow(hspace, cmap=plt.cm.bone, extent=(np.rad2deg(angles[-1]), \
#     np.rad2deg(angles[0]), distances[-1], distances[0]))
# axes[1].set_title('Hough Transform')
# axes[1].set_xlabel('Angle (degree)')
# axes[1].set_ylabel('Distance (pixel)')
# 
# axes[2].imshow(canny_image, cmap=cm.gray)
# for _, angle, dist in zip(hspace_peaks, angles_peaks, distances_peaks):
#     y0 = (dist - 0 * np.cos(angle)) / np.sin(angle)
#     y1 = (dist - canny_image.shape[1] * np.cos(angle)) / np.sin(angle)
#     axes[2].plot((0, canny_image.shape[1]), (y0, y1), 'r')
# axes[2].set_xlim((0, canny_image.shape[1]))
# axes[2].set_ylim((canny_image.shape[0], 0))
# axes[2].set_axis_off()
# axes[2].set_title('Detected lines')
# 
# plt.tight_layout()
# plt.show()
# =============================================================================
