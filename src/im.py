import matplotlib.pyplot as plot

# read and display an image (a)
eyeImage = plot.imread('eye.png')
plot.imshow(eyeImage)

# calculate the size of the image (b)
eyeSize = eyeImage.shape
eyePixels = eyeImage.size

# calculate the maximum pixel intensity value (c)
eyeMax = eyeImage.max()

# calculate the mean pixel intensity value (d)
eyeMean = eyeImage.mean()

# change the pixel values of the image to 0 and 1 (e)
maskZero = eyeImage < eyeMean
eyeImage[maskZero] = 0

maskOne = eyeImage >= eyeMean
eyeImage[maskOne] = 1

# uncomment this line to see the binary image created
#plot.imshow(eyeImage)

# prints the matrices for the image
#print(eyeImage)

# uncomment to print the values calculated above
#print(eyeSize)
#print(eyePixels)
#print(eyeMax)
#print(eyeMean)
